﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Common
{
    public class MPoint
    {
        int x, y, id;
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public MPoint(XElement element)
        {
            x = int.Parse(element.Descendants("Coordinate").Where(g => g.Attribute("Axis").Value == "X").First().Value);
            y = int.Parse(element.Descendants("Coordinate").Where(g => g.Attribute("Axis").Value == "Y").First().Value);
            id = int.Parse(element.Descendants("ID").First().Value);
        }

        public MPoint(int xcoord, int ycoord)
        {
            x = xcoord;
            y = ycoord;
            id = -1;
        }

        public override bool Equals(object obj)
        {
            MPoint p = (MPoint)obj;
            return x == p.X && y == p.Y;
        }
    }
}
