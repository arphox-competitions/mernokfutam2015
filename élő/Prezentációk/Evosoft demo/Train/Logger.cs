﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Train
{
    static class Logger
    {
        public static void InitializeLogFile()
        {
            string d = Directory.GetCurrentDirectory();
            d = d.Substring(0, d.Length - 15);
            d += "TrainTracker\\bin\\Debug\\log.txt";
            using (StreamWriter sw = new StreamWriter(d))
            {
                sw.Write("");
            }
        }
        public static void Log(string message)
        {
            string d = Directory.GetCurrentDirectory();
            d = d.Substring(0, d.Length - 15);
            d += "TrainTracker\\bin\\Debug\\log.txt";
            using (StreamWriter sw = new StreamWriter(d, true))
            {
                sw.WriteLine(message);
            }
        }
    }
}
