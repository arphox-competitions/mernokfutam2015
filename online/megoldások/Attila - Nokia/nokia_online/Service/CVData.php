<?php

include '../Converter/DocxConversion.php';
include '../Converter/pdf2text.php';

class CVData {

    public $cvContent   = "";
    private $path       = "";
    private $onlyPath   = "";
    public $fileName    = "";
    public $cvEmails    = "";
    public $points;
    public $isWeb       = false;
    
    public function __construct($cv_folder, $cv_fileName) {
        $this->path = $cv_folder."".$cv_fileName;
        $this->onlyPath = $cv_folder;

        $this->fileName = $cv_fileName;
        
        $this->codeCV();
        $this->points = 0;
    }

    private function codeCV() {
        $temp_arr = explode (".", $this->fileName);
        $file_extension = strtolower ($temp_arr[count($temp_arr) - 1]);
        switch ($file_extension) {
            case "pdf":
                $this->getTextFromPDF ($this->path);
                break;

            case "doc":
                $this->getTextFromDOCX ($this->path);
                break;

            case "docx":
                $this->getTextFromDOCX ($this->path);
                break;

            case "eml":
                //".eml feldolgozas meg nincs implementalva";
                break;
            case "fromnet":
                
                $this->isWeb = true;
                $this->fileName = $this->onlyPath;
                $this->getTextFromNet ($this->fileName);

                break;
            default:
                //"Error";
                break;
        }

        $this->cvEmails = $this->getEmailFromText ($this->cvContent);
        //echo $this->cvEmails[0];
        $this->cvContent = strtolower($this->cvContent);
    }

    private function getTextFromNet ($path) {
        $userPage = file_get_contents($path);
        $this->cvContent = $userPage;
    }
    
    private function getTextFromPDF ($path) {
        $pdfToText = new PDF2Text ();
        $pdfToText->setFilename ($path);
        $pdfToText->decodePDF ();
        $this->cvContent = $pdfToText->output ();
    }

    private function getTextFromDOCX($path) {
        $docToText = new DocxConversion($path);
        $this->cvContent = $docToText->convertToText ();
    }

    private function getEmailFromText ($string) {
        $emails = array ();
        $string = str_replace ("\r\n", ' ', $string);
        $string = str_replace ("\n", ' ', $string);

        foreach (preg_split('/ /', $string) as $token) {
            $email = filter_var ($token, FILTER_VALIDATE_EMAIL);
            if ($email !== false) {
                $emails[] = $email;
            }
        }
        return $emails;
    }
    
    public function incrementPoint ($points)
    {
        $this->points += $points;
    }
    
    public function setCvContent ($value) {
        $this->cvContent = $value;
    }
}
?>