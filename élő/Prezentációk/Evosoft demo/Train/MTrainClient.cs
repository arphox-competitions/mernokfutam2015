﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Train
{
    class MTrainClient
    {
        int x, y, speed, doorState, roadWorkStop; //doorState 1 = open, 0 = closed

        public int RoadWorkStop
        {
            get { return roadWorkStop; }
            set { roadWorkStop = value; }
        }

        int prevX, prevY, nextX, nextY;

        public int NextY
        {
            get { return nextY; }
            set { nextY = value; }
        }

        public int NextX
        {
            get { return nextX; }
            set { nextX = value; }
        }

        public int PrevY
        {
            get { return prevY; }
            set { prevY = value; }
        }

        public int PrevX
        {
            get { return prevX; }
            set { prevX = value; }
        }

        public int DoorState
        {
            get { return doorState; }
            set { doorState = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int Speed
        {
            get { return speed; }
        }

        public MTrainClient(int xcoord, int ycoord, int s)
        {
            x = xcoord;
            y = ycoord;
            speed = s;
            doorState = 1;
        }
    }
}
