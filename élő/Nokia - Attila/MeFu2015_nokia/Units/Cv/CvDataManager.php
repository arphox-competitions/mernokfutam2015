<?php
include_once './UserDataExtractor.php';
include_once './CvData.php';
include_once './PdfText.php';
include_once './DocText.php';
include_once './EFileType.php';
include_once './IGetCvDatas.php';
include_once './FileFromEml.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CvDataManager
 *
 * @author Matesz
 */
class CvDataManager implements IGetCvDatas {
    private $path;
    private $documents;
    private $tempFilePath;
    
    private $cvDatas;
    private $requiredKeyWords;
    
    public function __construct ($path ,$requiredKeyWords)
    {
        
        $this->documents = array();
        $this->cvDatas   = array();
        $this->requiredKeyWords = $requiredKeyWords;
        $this->path = $path;
        
        $this->tempFilePath = "";
        
        $this->ReadSelectedDir ($this->path);
        $this->GetDatasFromDoc();
        
        
    }
    
    private function ReadSelectedDir ($path) 
    {
        $res = opendir($path);

        while (false !== ($file = readdir($res))) {
            //if (strpos($file, 'doc') !== false || strpos($file, 'pdf') !== false) {
            if (  preg_match("/.pdf$/i", $file)
               || preg_match("/.docx$/i", $file)     
               || preg_match("/.doc$/i", $file)
               || preg_match("/.eml$/i", $file)
               )
            {
                array_push($this->documents, $path . "/" . $file);
            } else {
                if ($file != "..." && $file != ".." && $file != ".") {
                    if (is_dir($path . '/' . $file)) {
                        $this->ReadSelectedDir($path. '/' .$file);
                    }
                }
            }
        }
        closedir($res);
    }
    
    private function GetDatasFromDoc ()
    {
        
        foreach ($this->documents as $cvValue) {
            //$content = file_get_contents($cvValue);
            $this->StartMakeOfCvData($cvValue);
        }
    }
    
    private function GetFileType ($file)
    {
        $temp_arr = explode (".", $file);
        $file_extension = strtolower ($temp_arr[count($temp_arr) - 1]);
        switch ($file_extension) {
            case "pdf":
                return EFileType::Pdf;
                break;

            case "doc":
                return EFileType::DocDocx;
                break;

            case "docx":
                return EFileType::DocDocx;
                break;

            case "eml":
                return EFileType::Eml;
                break;
            
            default:
                //"Error";
                break;
        }
    }
    
    private function GetFileContent ($file, $fileType)
    {
        switch ($fileType) {
            case EFileType::DocDocx:
                $docTextem = new DocText($file);
                
                //Ez azert kell mert elszurtam a tervezest.:(
                $this->tempFilePath = $file;
                return $docTextem->GetContent ($this->tempFilePath);
                        
            case EFileType::Pdf:             
                $pdfTextem = new PdfText ();
                
                //Ez azert kell mert elszurtam a tervezest.:(
                $this->tempFilePath = $file;
                return $pdfTextem->GetContent ($this->tempFilePath);
            
            case EFileType::Eml:
                $fileFromEml = new FileFromEml ();
                
                $newFile = $fileFromEml->GetFileFromEml($file);
                $newType = $this->GetFileType ($newFile);
                
                //Ez azert kell mert elszurtam a tervezest.:(
                $this->tempFilePath = $newFile;
                return $this->GetFileContent ($newFile, $this->tempFilePath);
                
            default:
                break;
        }
    }
    
    private function StartMakeOfCvData ($cvValue)
    {
        $fileType = $this->GetFileType ($cvValue);
            
        $content = $this->GetFileContent($cvValue, $fileType);
            
        $this->MakeCvData ($content, $this->tempFilePath);
    }


    private function MakeCvData ($content, $cvValue)
    {
            $userDataExtractor = new UserDataExtractor ($content);
            
            $emails       = $userDataExtractor->GetValues(EUserDataType::Email);
            $phoneNumbers = $userDataExtractor->GetValues(EUserDataType::Phone);
            $keyWords     = $userDataExtractor->GetValues(EUserDataType::KeyWord, $this->requiredKeyWords);
            
            $fileName = explode("/", $cvValue);
            $fileName = $fileName[count ($fileName)-1];

            $folder   = str_replace( "/" . $fileName
                                   ,""
                                   , $cvValue
                                   );

            $data = new CvData( $folder
                              , $fileName
                              , $emails
                              , $phoneNumbers
                              , $keyWords
                              );
            
            array_push($this->cvDatas , $data); 
            //var_dump ($data);
            //echo "<br><br><br>";
    }

    public function GetCvDatas() {
        return $this->cvDatas;
    }

}
