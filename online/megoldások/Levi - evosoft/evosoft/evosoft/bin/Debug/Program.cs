﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ConsoleApplication4
{
    /*
     person>
<name>Balázs Éva</name>
<email>balazs.eva@nik.uni-obuda.hu</email>
<dept>Dékáni Hivatal</dept>
<rank>igazgatási ügyintéző</rank>
<phone>+36 (1) 666-5520</phone>
<room>BA.4.06</room>
</person>*/

    class Person
    {
        public string Name { get; set; }   //autoproperty 
        public string Email { get; set; }
        public string Dept { get; set; }
        public string Rank { get; set; }
        public string Phone { get; set; }
        public string Room { get; set; }

        public Person(XElement personElement)
        {
            this.Name = personElement.Element("name").Value;
            this.Email = personElement.Element("email").Value;
            this.Dept = personElement.Element("dept").Value;
            this.Rank = (string)personElement.Element("rank");
            this.Phone = personElement.Element("phone").Value;
            this.Room = personElement.Element("room").Value;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            XDocument xDoc = 
                XDocument.Load("http://users.nik.uni-obuda.hu/hp/people.xml");

            //Select    kiinduló lista -> eredménylistát 
            //          XElementek     -> Person-ök 

            IEnumerable<Person> eredmeny =
                xDoc.Root.Elements("person").Select(x => new Person(x));

            List<Person> workers = eredmeny.ToList();

//Határozzuk meg az AII intézetben dolgozók darabszámát!

            string intezet = "Alkalmazott Informatikai Intézet";
            var q = from x in workers
                    where x.Dept == intezet
                    select x;
            Kiir(q);

//Az AII dolgozóit listázzuk „lapozva”, 15 elemenként kelljen ENTER-t leütni
//Jelenítsük meg azokat, akiknek a harmadik emeleten van irodája!

            var q2 = from x in workers
                     where x.Room.StartsWith("BA.3")
                     select x;
            Kiir(q2);

//Kiknek van a leghosszabb vagy legrövidebb nevük?
            var q3 = from x in workers
                     let minLength = workers.Min(y => y.Name.Length)
                     let maxLength = workers.Max(y => y.Name.Length)
                     where x.Name.Length == minLength
                        || x.Name.Length == maxLength
                     select x;
            Kiir(q3);

//Határozzuk meg intézetenként a dolgozók darabszámát!

            var q4 = from x in workers
                     group x by x.Dept into g
                     select new { Intezet = g.Key, Letszam = g.Count() };
            Kiir(q4);


//Határozzuk meg a legnagyobb intézetet!
            var q5 = from i in q4
                     let maxIntezetLetszam = q4.Max(y => y.Letszam)
                     where i.Letszam == maxIntezetLetszam
                     select i;
            Kiir(q5);

//Listázzuk a legnagyobb intézet dolgozóit!
            var q6 = from x in workers
                     where q5.Select(i => i.Intezet).Contains(x.Dept)
                     select x;
            Kiir(q6);

//Listázzuk a harmadik legnagyobb intézet dolgozóit szobaszám szerint csökkenő sorrendben!
            //Skip() => kihagyok vhány elemet az eredményből, és visszaadja maradékot 
            var q7 = from x in workers 
                     where x.Dept == q4.OrderBy(i => i.Letszam).Reverse().Skip(2).First().Intezet
                     orderby x.Room descending
                     select x;
            Kiir(q7);

            Console.ReadLine();
        }

        static void Kiir<T>(IEnumerable<T> lista)
        {
            foreach (T elem in lista)
            {
                Console.WriteLine(elem);
            }
            Console.WriteLine("----------------------------------");
        }

    }
}
