﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace evosoft
{
    class Teleport
    {
        Cell end1, end2;
        public Teleport(XElement element)
        {
            end1 = element.Descendants("Coordinates").Select(x => new Cell(x)).First();
            end2 = element.Descendants("Coordinates").Select(x => new Cell(x)).Last();
        }

        public Cell Dst
        {
            get
            {
                return end2;
            }
        }

        public Cell Src
        {
            get
            {
                return end1;
            }
        }
    }
}
