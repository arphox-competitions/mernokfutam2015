/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function FilterJs(http)
{
    this.orGroups = [];
    this.orGroup = [];
    
    this.filter = [];
    this.loc = [];

    this.actualKeyWord = "";
    this.actualKeyWordPoint = 1;

    this.actualLoc = "";
    
    this.http = http;
}

FilterJs.prototype.addElementToOrGroup = function () {
    this.actualKeyWordPoint = parseInt(this.actualKeyWordPoint);
    if (this.actualKeyWord === ""
            || this.actualKeyWordPoint === ""
            || !Number.isInteger(this.actualKeyWordPoint)
            )
        return;

    this.orGroup.push({"word": this.actualKeyWord, "point": this.actualKeyWordPoint});
    
    this.actualKeyWord = "";
    this.actualKeyWordPoint = 0;
};

FilterJs.prototype.addToLoc = function ()
{
    if (this.actualLoc !== "")
    {
        this.loc.push(this.actualLoc);
        this.actualLoc ="";
    }
};

FilterJs.prototype.saveToGroup = function ()
{
    var tg = this.orGroup;
    this.orGroup = [];
    this.orGroups.push (tg);
};

FilterJs.prototype.reset = function ()
{
    alert(JSON.stringify(this.orGroups));
    this.orGroup = [];
    this.orGroups = [];
    this.loc = [];
    this.filter = [];
};


        




