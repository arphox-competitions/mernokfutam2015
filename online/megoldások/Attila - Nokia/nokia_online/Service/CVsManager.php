<?php

include 'CVData.php';

class CVsManager {

    private $relativePath = "../CVS/";
    public $pathArray = array();
    public $cvArray = array();
    private $keyWords = array();

    public function __construct($keyWords) {

        $this->keyWords = $keyWords;

        $this->getCVsFromFolder();
        
        //$this->getUserDatasFromWeb ();
        
         echo json_encode($this->cvArray);
    }

    private function getCVsFromFolder() {
        if ($handle = opendir('../CVS')) {

            while (false !== ($entry = readdir($handle))) {
                array_push($this->pathArray, $entry);
            }

            array_shift($this->pathArray);
            array_shift($this->pathArray);

            closedir($handle);
        }

        if (count($this->pathArray) > 0) {
            foreach ($this->pathArray as $path) {
                try {
                    $tempCVData = new CVData($this->relativePath, $path);
                    $this->getPointsByKeyWordsInDocs($tempCVData);

                    array_push($this->cvArray, $tempCVData);
                } catch (Exception $ex) {
                   echo " //Nem tudja ertelmezni a fajlt.";
                }
            }

        }
    }

    private function getUserDatasFromWeb() {

        $query = 'https://github.com%20"budapest"%20"hungary"%20"contributions"%20-"lines"%20("java"%20|%20"j2ee")';
        $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=" . $query;

        $body = file_get_contents($url);
        $json = json_decode($body);
        try {
            foreach ($json->responseData->results as $result) {

 
                    $tempCVData = new CVData($result->url, "a.fromNet");

                    $this->getPointsByKeyWordsInDocs($tempCVData);

                    array_push($this->cvArray, $tempCVData);

            }
        } catch (Exception $ex) {
                
        }
    }

    public function getPointsByKeyWordsInDocs($cvData) {
        foreach ($this->keyWords as $valami) {
            if (strpos($cvData->cvContent, $valami->word) !== false) {
                $cvData->incrementPoint($valami->point);
            }
            
            
        }
        $cvData->setCvContent("");
    }

}
