﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace evosoft
{
    class FileProcesser
    {
        public FileProcesser()
        {
            string[] tomb = Directory.GetFiles(Directory.GetCurrentDirectory(), "input_*.xml", SearchOption.TopDirectoryOnly);
            foreach (string item in tomb)
            {
                List<Cell> pathResult = Process(item).Distinct().ToList();
                pathResult.Reverse();
                //Console.WriteLine("\nPath Cells");
                //foreach (Cell item2 in pathResult)
                //{
                //        Console.WriteLine(" X: " + item2.X + " Y: " + item2.Y + " Value: " + item2.Value);
                //}
                string outputFilename = "output_" + item.Substring(item.IndexOf("input_") + 6, item.IndexOf('.') - (item.IndexOf("input_") + 6)) + ".txt";
                if (pathResult.Count > 1)
                {
                    using (StreamWriter sw = new StreamWriter(outputFilename))
                    {
                        for (int i = 0; i < pathResult.Count; i++)
                        {
                            sw.Write("({0},{1})" + (i == pathResult.Count - 1 ? "" : ";"), pathResult[i].X, pathResult[i].Y);
                        }
                    }
                }
                else
                {
                    using (StreamWriter sw = new StreamWriter(outputFilename))
                    {
                        sw.Write("NM");
                    }
                }
                //Console.WriteLine();
            }
        }

        List<Cell> Process(string filename)
        {
            XDocument xDoc = XDocument.Load(filename);
            int Width = int.Parse(xDoc.Root.Attribute("Width").Value);
            int Height = int.Parse(xDoc.Root.Attribute("Height").Value);
            List<Cell> cells = xDoc.Root.Descendants("Cell").Select(x => new Cell(x)).ToList();
            List<Cell> walls = xDoc.Root.Descendants("Wall").Select(x => new Cell(x)).ToList();
            List<Cell> checkpoints = null;
            List<Teleport> teleports = null;
            if (xDoc.Root.Descendants("Teleport").ToList().Count != 0)
            {
                teleports = xDoc.Root.Descendants("Teleport").Select(x => new Teleport(x)).ToList();
            }
            if (xDoc.Root.Descendants("Checkpoint").ToList().Count != 0)
            {
                checkpoints = xDoc.Root.Descendants("Checkpoint").Select(x => new Cell(x)).ToList();
            }
            cells = cells.Concat(walls).ToList();
            Cell start = xDoc.Root.Descendants("Start").Select(x => new Cell(x)).First();
            Cell finish = xDoc.Root.Descendants("Finish").Select(x => new Cell(x)).First();
            PathFinder pf = new PathFinder(cells, Width, Height, start, finish, checkpoints, teleports);
            //pf.Kiir();
            return pf.ShortestPath;
        }
    }
}
