﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;

namespace Control
{
    //Template from MSDN
    static class Server
    {
        public static Map Map;
        static Random rnd = new Random();
        static bool reachedFinalDestination;
        static int remainingDwellTime;
        // Incoming data from the client.
        public static string data = null;

        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // Dns.GetHostName returns the name of the 
            // host running the application.
            IPHostEntry ipHostInfo = new IPHostEntry();
            byte[] ipaddress = { 127, 0, 0, 1 };
            ipHostInfo.AddressList = new IPAddress[1];
            ipHostInfo.AddressList[0] = new IPAddress(ipaddress);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.
                while (!reachedFinalDestination)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.
                    while (true)
                    {
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf("$") > -1)
                        {
                            break;
                        }
                    }

                    // Show the data on the console.
                    Console.WriteLine("Text received : {0}", data);
                    data = CreateMessage(data);
                    // Echo the data back to the client.
                    byte[] msg = Encoding.ASCII.GetBytes(data);

                    handler.Send(msg);
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        static string CreateMessage(string incomingMessage)
        {
            string responseMessage = String.Empty;
            if (incomingMessage[0] == 'r')
            {
                MStation station = null;
                int rng = rnd.Next(0, Map.Roads.Count);
                MRoad road = Map.Roads[rng];
                int distance = Math.Abs(road.StartPoint.X - road.EndPoint.X) + Math.Abs(road.StartPoint.Y - road.EndPoint.Y);
                if (road.StartPoint.X != road.EndPoint.X)
                {
                    if (road.StartPoint.X < road.EndPoint.X)
                    {
                        rng = rnd.Next(road.StartPoint.X + 1, road.EndPoint.X);
                    }
                    else
                    {
                        rng = rnd.Next(road.EndPoint.X + 1, road.StartPoint.X);
                    }
                    station = new MStation(rng, road.EndPoint.Y);
                }
                else
                {
                    if (road.StartPoint.Y < road.EndPoint.Y)
                    {
                        rng = rnd.Next(road.StartPoint.Y + 1, road.EndPoint.Y);
                    }
                    else
                    {
                        rng = rnd.Next(road.EndPoint.Y + 1, road.StartPoint.Y);
                    }
                    station = new MStation(road.EndPoint.X, rng);
                }
                responseMessage = String.Format("{0},{1},{2}", 'w', station.StationPoint.X, station.StationPoint.Y);
                Map.Stations.Add(station);
                int index = Map.Path.IndexOf(road.StartPoint);
                Map.Path.Insert(index + 1, station.StationPoint);
                return responseMessage;
            }
            if (incomingMessage[0] == 'i')
            {
                Map.Train.X = Map.Path[0].X;
                Map.Train.Y = Map.Path[0].Y;
                Map.CurrentDstPointIndex = 0;
                responseMessage = String.Format("i,{0},{1},{2}", Map.Path[0].X, Map.Path[0].Y, Map.Train.Speed);
            }
            else
            {
                incomingMessage = incomingMessage.Substring(0, incomingMessage.Length - 1);
                string[] s = incomingMessage.Split(',');
                Map.Train.X = int.Parse(s[0]);
                Map.Train.Y = int.Parse(s[1]);
                responseMessage = DetermineCommand(incomingMessage);
            }
            return responseMessage;
        }

        static string DetermineCommand(string incomingMessage)
        {
            string command = String.Empty;
            string[] s = incomingMessage.Split(',');
            MPoint trainPosition = new MPoint(Map.Train.X, Map.Train.Y);
            #region CommandLogic
            
            if (trainPosition.Equals(Map.Path[Map.CurrentDstPointIndex]))
            {
                if (Map.CurrentDstPointIndex != 0)
                {
                    if (isStation(Map.Path[Map.CurrentDstPointIndex]))
                    {
                        if (Map.CurrentDstPointIndex == Map.Path.Count - 1)
                        {
                            command = "b";
                            reachedFinalDestination = true;
                        }
                        else
                        {
                            MStation station = GetStationFromPath();
                            if (!station.IsRoadWork)
                            {
                                if (int.Parse(s[s.Length - 2]) == 0)
                                {
                                    if (remainingDwellTime != -1)
                                    {
                                        command = "o";
                                        remainingDwellTime = station.DwellTime;
                                    }
                                    else
                                    {
                                        Map.CurrentDstPointIndex++;
                                        command = String.Format("{0},{1},{2}", 'm', Map.Path[Map.CurrentDstPointIndex].X, Map.Path[Map.CurrentDstPointIndex].Y);
                                        remainingDwellTime = 0;
                                    }
                                }
                                else
                                {
                                    if (remainingDwellTime > 1)
                                    {
                                        remainingDwellTime--;
                                        command = "x";
                                    }
                                    else
                                    {
                                        command = "c";
                                        remainingDwellTime = -1;
                                    }
                                }
                            }
                            else
                            {
                                if (int.Parse(s[s.Length - 1]) == 0)
                                {
                                    if (remainingDwellTime != -1)
                                    {
                                        command = "r";
                                        remainingDwellTime = station.DwellTime;
                                    }
                                    else
                                    {
                                        Map.CurrentDstPointIndex++;
                                        command = String.Format("{0},{1},{2}", 'm', Map.Path[Map.CurrentDstPointIndex].X, Map.Path[Map.CurrentDstPointIndex].Y);
                                        remainingDwellTime = 0;
                                    }
                                }
                                else
                                {
                                    if (remainingDwellTime > 1)
                                    {
                                        remainingDwellTime--;
                                        command = "x";
                                    }
                                    else
                                    {
                                        command = "q";
                                        remainingDwellTime = -1;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Map.CurrentDstPointIndex++;
                        command = String.Format("{0},{1},{2}", 'm', Map.Path[Map.CurrentDstPointIndex].X, Map.Path[Map.CurrentDstPointIndex].Y);
                    }
                }
                else
                {
                    command = String.Format("{0},{1},{2}", 'a', Map.Path[1].X, Map.Path[1].Y);
                    Map.CurrentDstPointIndex++;
                }
            }
            else
            {
                int distanceFromNextDst = Math.Abs(Map.Path[Map.CurrentDstPointIndex].X - Map.Train.X) + Math.Abs(Map.Path[Map.CurrentDstPointIndex].Y - Map.Train.Y);
                if (distanceFromNextDst < Map.Train.Speed)
                {
                    if (!isStation(Map.Path[Map.CurrentDstPointIndex]))
                    {
                        command = String.Format("{0},{1},{2},{3}", 'n', Map.Path[Map.CurrentDstPointIndex + 1].X, Map.Path[Map.CurrentDstPointIndex + 1].Y, 'p');
                        Map.CurrentDstPointIndex++;
                    }
                    else
                    {
                        if (Map.CurrentDstPointIndex != Map.Path.Count - 1)
                        {
                            command = String.Format("{0},{1},{2},{3}", 'n', Map.Path[Map.CurrentDstPointIndex + 1].X, Map.Path[Map.CurrentDstPointIndex + 1].Y, 's');
                            Map.CurrentDstPointIndex++;
                        }
                        else
                        {
                            command = "z";
                        }
                    }
                }
                else
                {
                    command = "x";
                }
            }
            #endregion
            return command;
        }


        static bool isStation(MPoint point)
        {
            foreach (MStation station in Map.Stations)
            {
                if (station.StationPoint.Equals(point))
                {
                    return true;
                }
            }
            return false;
        }

        static MStation GetStationFromPath()
        {
            MStation station = null;
            foreach (MStation s in Map.Stations)
            {
                if (s.StationPoint.Equals(Map.Path[Map.CurrentDstPointIndex]))
                {
                    station = s;
                }
            }
            return station;
        }

        static void CreateRandomRoadWork()
        {

        }
    }
}
