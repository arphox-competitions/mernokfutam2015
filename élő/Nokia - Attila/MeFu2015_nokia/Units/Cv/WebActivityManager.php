<?php
include_once './CvData.php';
include_once './UserDataExtractor.php';
include_once './IGetCvDatas.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebActivityManager
 *
 * @author Matesz
 */
class WebActivityManager implements IGetCvDatas {
    
    private $cvDatas;
    private $requiredKeyWords;
    private $locs;
    private $onlyKeyWords;
    
    private $filterString;
    private $locString;
    private $locAndString;
    
    //put your code here
    public function __construct ($keywords, $locs, $onlyKeywords) {
        $this->cvDatas = array ();
        $this->requiredKeyWords = $keywords;
        $this->onlyKeyWords = $onlyKeywords;
      
        $this->locs = $locs;
        
        $this->filterString = $this->GetFilterString ();
        $this->locString    = $this->GetLocString ();
        $this->locAndString = $this->GetLocAndString ();
        try{
            $this->GetLinkedIn ();
            $this->GetStackOverflow ();
            $this->GetGitHub ();
        } catch (Exception $ex) {
            throw new Exception("Itt sincs internet.", 0, 0);
        }
        
    }
    
    private function GetFilterString ()
    {
        $filter = ""; 

       for ($index = 0; $index < count ($this->requiredKeyWords); $index++) {
           
           $filter .=" ( ";
           
           for ($index1 = 0; $index1 < count ($this->requiredKeyWords[$index]); $index1++) {
                if ($index1 == count($this->requiredKeyWords[$index])-1)
                {
                    $filter .= $this->requiredKeyWords[$index][$index1]->word." ";
                }else{
                    $filter .= $this->requiredKeyWords[$index][$index1]->word." | ";
                }
           }
           
           if ($index == count($this->requiredKeyWords)-1)
           {
               $filter .=" ) ";
           }else{
               $filter .=" ) & ";
           }
           
       }
      return $filter;
    }
    
    private function GetLocString ()
    {
        $loc = "";
        foreach ($this->locs as $value) {
            $loc .=" ".$value." ";
        }
        return $loc;
    }
    
    private function GetLocAndString ()
    {
        $filter =" & ( ";
           for ($index = 0; $index < count ($this->locs); $index++) {
                if ($index == count($this->locs)-1)
                {
                    $filter .= $this->locs[$index]." ";
                }else{
                    $filter .= $this->locs[$index]." | ";
                }
           }
           
           $filter .=" ) ";
           return $filter;
    }
    
    
    private function GetLinkedIn ()
    {
       
        $query = "hu.linkedin.com/ ".$this->filterString." ".$this->locAndString." -inurl:jobs -inurl:company -inurl:topic -inurl:title -inurl/pub/dir -HR";
        
        //echo $query;
        $keres = str_replace(" ","%20",$query);
        $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=$keres&start=0";

        try{
            $content = file_get_contents($url);
        } catch (Exception $ex) {
            throw new Exception("Nincs internet", 0, 0);
        }
        
        $json = json_decode($content);
        
        foreach ($json->responseData->results as $searchResult) {
            $content = file_get_contents($searchResult->url);
            
            $name = $this->GetLinkedInName ($searchResult->title);
            
            $this->GetWebDatas ($content, $searchResult->url, $name);
        }
    }
    
    private function GetWebDatas ($content, $url ,$name = "")
    {
        $userDataExtractor = new UserDataExtractor ($content);
            
        $emails       = $userDataExtractor->GetValues(EUserDataType::Email);
        $phoneNumbers = $userDataExtractor->GetValues(EUserDataType::Phone);
        $keyWords     = $userDataExtractor->GetValues(EUserDataType::KeyWord, $this->onlyKeyWords);

        $data = new CvData( ""
                           , $url
                           , $emails
                           , $phoneNumbers
                           , $keyWords
                           , $name
                           );

        array_push($this->cvDatas , $data); 
        //var_dump ($data);
        //echo "<br><br><br>";
    }
    
    private function GetLinkedInName ($nameString)
    {
        $names = explode(" ", $nameString);
        $name = "";
        foreach ($names as $value) {
            if ($value == "|")
                break;
            
            $name .= $value. " ";
        }
        return $name;
    }
    
    private function GetStawkOverflowName ($nameString)
    {
        $names = explode(" ", $nameString);
        $name = "";
        foreach ($names as $value) {
            if ($value == "-")
                break;
            
            if ($value != "User")
                $name .= $value. " ";
        }
        return $name;
    }
    
    private function GetGitHubName ($nameString)
    {
        $pos1 = strpos($nameString, "(");
        $pos1 += 1;
        $pos2 = strpos($nameString, ")");
        
        return substr($nameString, $pos1, ($pos2-$pos1));
    }
    
    private function GetStackOverflow ()
    {
        $tempString = substr_replace($this->filterString,"",0,1);
        
        //$keres = str_replace(" ","%20","http://stackoverflow.com/users/ intext:(budapest | java | hungary | php)");
         $query = "http://stackoverflow.com/users/ intext:".$tempString. " " . $this->locAndString;
        
        //echo $query;
        $keres = str_replace(" ","%20",$query);
        $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=$keres&start=0";
        
        try{
            $content = file_get_contents($url);
        } catch (Exception $ex) {
            throw new Exception("Nincs internet", 0, 0);
        }
        $json = json_decode($content);
        //var_dump($json->responseData->results);
        
        foreach ($json->responseData->results as $searchResult) {
            $content = file_get_contents($searchResult->url);
            
            $name = $this->GetStawkOverflowName ($searchResult->title);
            
            $this->GetWebDatas ($content,$searchResult->url,$name);
        }
    }
    
    private function GetGitHub ()
    {
        
        $query = "https://github.com ".$this->locString." contributions -lines ".$this->filterString;
        
        //echo $query;
        //$keres = str_replace(" ","%20","https://github.com budapest hungary contributions -lines ( java | c# | php  )");
        $keres = str_replace(" ","%20",$query);
        
        $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=$keres&start=2";
        
        try{
            $content = file_get_contents($url);
        } catch (Exception $ex) {
            throw new Exception("Nincs internet", 0, 0);
        }
        $json = json_decode($content);
        //var_dump($json->responseData->results);
        
        foreach ($json->responseData->results as $searchResult) {
            $content = file_get_contents($searchResult->url);
            
            $name = $this->GetGitHubName ($searchResult->title);
            
            $this->GetWebDatas ($content,$searchResult->url, $name);
        }
    }

    public function GetCvDatas() {
        return $this->cvDatas;
    }

}
