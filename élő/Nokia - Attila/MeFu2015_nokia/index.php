<?php
ob_start();
session_start();
if (!isset($_SESSION["username"])) {
    header("Location: login.php");
}
?>

<html>
    <head>
        <title>CV Master 4000</title>

        <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!--Own scripts-->
        <script src="Js/FilterJs.js"></script>
        <script src="JS/app.js"></script>

        <script>
            $(document).ready(function () {
                $("#teszt").click(function () {
                    $("#kulcsSzavakTablazatTorzs").toggle("fast");
                });
            });
        </script>

        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1>Cv Master 4000</h1>
                <p>:)</p> 
            </div>
            <div class="row">
                <div>
                    <?php echo "Üdvözöljük " . $_SESSION["username"]; ?>
                    <a href="logout.php"> Kijelentkezés </a>

                </div>
            </div>

            </br>
            <div class="row" ng-app="myApp" ng-controller="myCtrl">
                <div class="col-sm-4">
                    <div >
                        <table>
                            <tr>
                                <td width='100'>
                                    Kulcsszó:
                                </td>
                                <td width='100'>
                                    <input type="text" ng-model="filterOptions.actualKeyWord"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Prioritás:
                                <td>
                                    <input type="text" ng-model="filterOptions.actualKeyWordPoint"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="float:left;">
                                        <button ng-click="filterOptions.addElementToOrGroup()">Add to group</button>
                                        <button ng-click="filterOptions.saveToGroup()">save</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="row">

                        </div>
                    </div>
                    </br>



                </div>
                <div class="col-sm-4">
                    <table>
                        <tr>
                            <td width='100'>
                                Lakhely:
                            </td>
                            <td>
                                <input type="text" ng-model="filterOptions.actualLoc"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>

                                <button ng-click="filterOptions.addToLoc()">Hozzáad</button>
                            </td>
                        </tr>
                    </table>    
                </div>
                <div class="col-sm-4">
                    <div style="float:left;">


                        <button ng-click="startSearch()">Keresés</button>
                        <button ng-click="filterOptions.reset()">Filter törlés</button>
                    </div>
                </div>         


                <div class="row">

                </div>
                <div class="row">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><span>Kulcsszavak  <span id="teszt" style="cursor:pointer; width: 30px; height: 30px; font-size: 20px;">+/-</span></span></th>
                            </tr>
                        </thead>
                        <tbody id="kulcsSzavakTablazatTorzs">
                            <tr ng-click="" ng-repeat="groups in filterOptions.orGroups">

                                <td>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Kulcsszó</th>
                                                <th>Prioritás</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-click="" ng-repeat="group in groups">

                                                <td>{{ group.word}}</td>
                                                <td>{{ group.point}}</td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Hely</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-click="" ng-repeat="locals in filterOptions.loc">
                                <td>{{locals}}</td>
                            </tr>

                        </tbody>
                    </table>

                    <table class="table table-striped">
                        <tr ng-click="" ng-repeat="result in filterResult | orderBy:'-score'">

                            <td><a href='{{ result.baseData.path}}' target="_blank">Doc<a></td>
                            <td>{{ result.cvName}}</td>
                            <td>{{ result.score}}</td>
                        </tr>


                    </table>
                </div>
            </div>  
            <div class="row">
                <div class="col-sm-4">
                    <h3>Egyszerű</h3>
                    <p>Csak írja be a kulcsszavakat.</p>
                    <p>Adja meg mennyire érdekes az adott kulcsszó.</p>
                </div>
                <div class="col-sm-4">
                    <h3>Gyors</h3>
                    <p>Szerver oldali keresések</p>
                </div>
                <div class="col-sm-4">
                    <h3>Hatékony</h3>        
                    <p>A pc-n lévő cvkből kiválasztja a legjobbakat.</p>
                </div>
            </div>
        </div>

    </body>
</html>
