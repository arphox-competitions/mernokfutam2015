var app = angular.module('myApp', []);
app.controller('customersCtrl', function ($scope, $http) {
    $scope.toggle = function(item) {
        //alert(JSON.stringify(item));
    };
    
    $scope.actualKeyWord = {"word":"" , "point":1};
    
    $scope.keyWordsList = [];
    $scope.deleteKeyWordsList = function () {
        $scope.keyWordsList = [];
        $scope.actualKeyWord = {"word":"" , "point":1};
    }
    $scope.addElementToKeyWordsList = function () {
        $scope.actualKeyWord.point = parseInt($scope.actualKeyWord.point);
        if (   $scope.actualKeyWord.word == "" 
            || $scope.actualKeyWord.point == ""
            || !Number.isInteger($scope.actualKeyWord.point)
           )
            return;
        
        $scope.keyWordsList.push ($scope.actualKeyWord);
        $scope.actualKeyWord = {"word":"" , "point":1};
    }
    
    $scope.startSearch = function () {
            var datasToSend = {KeyWords:$scope.keyWordsList};
            $http.post("http://localhost/nokia_online/service/CVBE.php",  datasToSend).success(function(data, status) {
                $scope.cvs = data;
            });
        }
    });