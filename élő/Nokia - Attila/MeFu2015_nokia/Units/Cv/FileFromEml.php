<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileFromEml
 *
 * @author Matesz
 */
class FileFromEml {
    
    public function __construct() {
        ;
    }
    
    public function GetFileFromEml ($file)
    {
        $newFileName = "";
        $filePathE = explode ("/",$file);
        $filePath = "";
        
        for ($index = 0; $index < count($filePathE)-1; $index++) {
            $filePath .= $filePathE[$index]."/";
        }
        //echo $filePath;
        
        $handle = file_get_contents($file);
        
        //echo $file;
        
        $isStartOfFile = false;
        $lineCounter = 0;
        $fileom = "";
        $fileName = "";

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $handle) as $line){

            $findme   = 'filename="';
            $pos = 0;
            $pos = strpos($line, $findme);

            if ($pos>0)
            {
                $rest = substr($line, $pos + 10, -1);    // returns "f"
                $fileName = $rest;
            }
            $findme   = 'Attachment-Id';
            $pos = 0;
            $pos = strpos($line, $findme);

            if ($pos>0)
            {
                $isStartOfFile = true;
            }

            if ($isStartOfFile)
            {
                if ($lineCounter > 1)
                {
                    $fileom .=$line."\n";
                }
                else
                {
                    $lineCounter++;
                } 
            }   

            if (strpos($line,"==") == true) {
                break;
            } 
        }


        //echo "<pre> $file </pre>";

        $current = base64_decode ($fileom);
        $newFileName = $fileName;
        if (file_exists($filePath."".$fileName))
        {
            $rand = rand();
            $newFileName = $rand.$newFileName;
            file_put_contents($filePath.$newFileName, $current);
        }else{
            file_put_contents($filePath.$fileName, $current);
        }
        
        $newFileName = $filePath."".$newFileName;
        //echo $newFileName;
        return $newFileName;
    }
}
