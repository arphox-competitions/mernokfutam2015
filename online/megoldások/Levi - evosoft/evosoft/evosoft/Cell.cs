﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace evosoft
{
    class Cell
    {
        int x, y, value;
        List<Cell> sources;

        public int Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }

        public List<Cell> Sources
        {
            get
            {
                return sources;
            }
        }

        public Cell(XElement element)
        {
            x = int.Parse(element.Descendants("Coordinate").Where(g => g.Attribute("Axis").Value == "X").First().Value);
            y = int.Parse(element.Descendants("Coordinate").Where(g => g.Attribute("Axis").Value == "Y").First().Value);
            if (element.Descendants("Value").FirstOrDefault() != null)
            {
                value = int.Parse(element.Element("Value").Value);
            }
            else
            {
                value = -1;
            }
            sources = new List<Cell>();
        }

        public Cell()
        {
            sources = new List<Cell>();
        }

        public override bool Equals(object obj)
        {
            Cell otherCell = (Cell)obj;
            return otherCell.X == this.X && otherCell.Y == this.Y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
