﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Common
{
    public class MRoad
    {
        int id;

        public int Id
        {
            get { return id; }
        }
        MPoint startPoint, endPoint;

        public MPoint EndPoint
        {
            get { return endPoint; }
        }

        public MPoint StartPoint
        {
            get { return startPoint; }
        }

        public MRoad(XElement element, List<MPoint> points)
        {
            id = int.Parse(element.Descendants("ID").First().Value);
            int sid = int.Parse(element.Descendants("Start_Point_ID").First().Value);
            int eid = int.Parse(element.Descendants("End_Point_ID").First().Value);
            startPoint = points.Single(x => x.Id == sid);
            endPoint = points.Single(x => x.Id == eid);
        }
    }
}
