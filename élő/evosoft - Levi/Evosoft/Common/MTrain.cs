﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Common
{
    public class MTrain
    {
        int id, speed, x, y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Id
        {
            get { return id; }
        }

        public int Speed
        {
            get { return speed; }
        }

        public MTrain(XElement element)
        {
            id = int.Parse(element.Descendants("ID").First().Value);
            speed = int.Parse(element.Descendants("Speed").First().Value);
        }
    }
}
