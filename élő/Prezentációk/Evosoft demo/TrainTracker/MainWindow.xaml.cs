﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Shapes;
using Common;

namespace TrainTracker
{
    public partial class MainWindow : Window
    {
        string[] rawFile = File.ReadAllLines("log.txt");
        Map map;
        int skála = 20; //ne írd át
        int MapMéret = 30;
        int currentRowInFile = 1;
        List<Point> points = new List<Point>();
        int currentPointIndex = 0;
        bool end = false;
        Brush trainSzín = Brushes.Violet;
        Brush trainStationSzín = Brushes.Orange;
        Brush trainProblemSzín = Brushes.Black;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Step()
        {
            if (!end)
            {
                string[] sor = rawFile[currentRowInFile++].Split(',');
                int x = int.Parse(sor[0]);
                int y = int.Parse(sor[1]);
                int harmadik = int.Parse(sor[2]);
                int negyedik = -1;

                if (sor.Length == 5) //van j
                {
                    //lépjünk le gyorsan a j-s sorra:
                    Point prev = points.Last();
                    DrawTrainLine((int)prev.X, (int)prev.Y, x, y);
                    points.Add(new Point(x, y));

                    //lépjünk gyorsan is tovább:
                    sor = rawFile[currentRowInFile++].Split(',');
                    int ujx = int.Parse(sor[0]);
                    int ujy = int.Parse(sor[1]);
                    DrawTrainLine(x, y, ujx, ujy);
                    DrawTrain(ujx, ujy);
                    points.Add(new Point(ujx, ujy));


                    return;
                }


                if (sor[3] == "b")
                {
                    end = true;
                }
                else
                {
                    negyedik = int.Parse(sor[3]);
                }

                if (points.Count > 0)
                {
                    Point prev = points.Last();
                    DrawTrainLine((int)prev.X, (int)prev.Y, x, y);
                }

                if (harmadik == 1)
                {
                    DrawTrainAtStation(x, y);
                }
                else if (negyedik == 1)
                {
                    DrawTrainAtProblem(x, y);
                }
                else
                {
                    DrawTrain(x, y);
                }
                points.Add(new Point(x, y));
            }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape: this.Close(); break;
                case Key.Space: Step(); break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            XmlProcesser processer = new XmlProcesser("input.xml");
            this.map = processer.ProcessXml();

            DrawGrid();

            DrawPaths();
            DrawPoints();
            DrawStationsRoadworks();

            //Draw RoadWorks from log:

            string[] sor = rawFile[0].Split(',');
            int x = int.Parse(sor[0]);
            int y = int.Parse(sor[1]);
            DrawRoadWork(x, y);


            label.Content =
                "Jelmagyarázat:\nFekete vonal - út\nZöld négyzet - állomás\nPiros négyzet - útépítés\n\nVonat:\nLila kör - haladó vonat\nNarancssárga kör - állomáson álló vonat\n(ajtók nyitva)\nFekete kör - útépítésnél álló vonat\nSárga vonal - a vonat megtett útja\n\nKék kör - Térképen jelölt pont\n\nLéptetés - Szóköz\nKilépés - Escape";
        }

        #region RácsRajzolás
        private void DrawRácsVonal(int x1, int y1, int x2, int y2)
        {
            //Skálázás:
            x1 *= skála;
            y1 *= skála;
            x2 *= skála;
            y2 *= skála;

            x1 += skála / 2;
            x2 += skála / 2;
            y1 += skála / 2;
            y2 += skála / 2;

            Line line = new Line();
            line.Stroke = Brushes.Gray;

            line.X1 = x1;
            line.X2 = x2;
            line.Y1 = y1;
            line.Y2 = y2;

            line.StrokeThickness = 1;
            canvas.Children.Add(line);
        }
        private void DrawGrid()
        {
            for (int i = 0; i < MapMéret; i++)
            {
                DrawRácsVonal(0, i, MapMéret - 1, i);
            }
            for (int i = 0; i < MapMéret; i++)
            {
                DrawRácsVonal(i, 0, i, MapMéret - 1);
            }
        }
        #endregion

        private void DrawPaths()
        {
            for (int i = 0; i < map.Path.Count - 1; i++)
            {
                MPoint akt = map.Path[i];
                MPoint next = map.Path[i + 1];
                DrawLine(akt.X, akt.Y, next.X, next.Y);
            }
        }
        private void DrawPoints()
        {
            for (int i = 0; i < map.Points.Count; i++)
            {
                MPoint akt = map.Points[i];
                DrawPoint(akt.X, akt.Y);
            }
        }
        private void RemoveElőzőTrain()
        {
            //Előző trainek kitörlése:
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i].GetType() == typeof(Ellipse))
                {
                    if (((Ellipse)canvas.Children[i]).Fill == trainSzín ||
                        ((Ellipse)canvas.Children[i]).Fill == trainStationSzín ||
                        ((Ellipse)canvas.Children[i]).Fill == trainProblemSzín)
                    {
                        canvas.Children.RemoveAt(i);
                        break;
                    }
                }
            }
        }
        private void DrawStationsRoadworks()
        {
            for (int i = 0; i < map.Stations.Count; i++)
            {
                MStation akt = map.Stations[i];
                if (akt.IsRoadWork)
                {
                    DrawRoadWork(akt.StationPoint.X, akt.StationPoint.Y);
                }
                else
                {
                    DrawStation(akt.StationPoint.X, akt.StationPoint.Y);
                }
            }
        }
        private void DrawTrain(int x, int y)
        {
            RemoveElőzőTrain();

            x *= skála;
            y *= skála;

            x -= skála / 4;
            y -= skála / 4;

            Ellipse e = new Ellipse();
            e.Height = 10;
            e.Width = 10;
            e.StrokeThickness = 1;
            e.Fill = trainSzín;
            e.Stroke = trainSzín;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }
        private void DrawTrainAtStation(int x, int y)
        {
            RemoveElőzőTrain();


            x *= skála;
            y *= skála;

            x -= skála / 4;
            y -= skála / 4;

            Ellipse e = new Ellipse();
            e.Height = 10;
            e.Width = 10;
            e.StrokeThickness = 1;
            e.Fill = trainStationSzín;
            e.Stroke = trainStationSzín;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }
        private void DrawTrainAtProblem(int x, int y)
        {
            RemoveElőzőTrain();


            x *= skála;
            y *= skála;

            x -= skála / 4;
            y -= skála / 4;

            Ellipse e = new Ellipse();
            e.Height = 10;
            e.Width = 10;
            e.StrokeThickness = 1;
            e.Fill = trainProblemSzín;
            e.Stroke = trainProblemSzín;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }

        private void DrawTrainLine(int x1, int y1, int x2, int y2)
        {
            //Skálázás:
            x1 *= skála;
            y1 *= skála;
            x2 *= skála;
            y2 *= skála;

            Line line = new Line();
            line.Stroke = Brushes.Yellow;

            line.X1 = x1;
            line.X2 = x2;
            line.Y1 = y1;
            line.Y2 = y2;

            line.StrokeThickness = 2;
            canvas.Children.Add(line);
        }

        private void DrawLine(int x1, int y1, int x2, int y2)
        {
            //Skálázás:
            x1 *= skála;
            y1 *= skála;
            x2 *= skála;
            y2 *= skála;

            Line line = new Line();
            line.Stroke = Brushes.Black;

            line.X1 = x1;
            line.X2 = x2;
            line.Y1 = y1;
            line.Y2 = y2;

            line.StrokeThickness = 2;
            canvas.Children.Add(line);
        }
        private void DrawPoint(int x, int y)
        {
            x *= skála;
            y *= skála;

            x -= skála / 4;
            y -= skála / 4;

            Ellipse e = new Ellipse();
            e.Height = 10;
            e.Width = 10;
            e.StrokeThickness = 1;
            e.Fill = Brushes.Blue;
            e.Stroke = Brushes.Blue;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }
        private void DrawStation(int x, int y)
        {
            x *= skála;
            y *= skála;

            x -= 7;
            y -= 7;

            Rectangle e = new Rectangle();
            e.Height = 14;
            e.Width = 14;
            e.StrokeThickness = 2;
            e.Fill = Brushes.Green;
            e.Stroke = Brushes.Green;

            //Ellipse e = new Ellipse();
            //e.Height = 10;
            //e.Width = 10;
            //e.StrokeThickness = 2;
            //e.Fill = Brushes.Green;
            //e.Stroke = Brushes.Green;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }
        private void DrawRoadWork(int x, int y)
        {
            x *= skála;
            y *= skála;

            x -= 7;
            y -= 7;

            Rectangle e = new Rectangle();
            e.Height = 14;
            e.Width = 14;
            e.StrokeThickness = 2;
            e.Fill = Brushes.Red;
            e.Stroke = Brushes.Red;

            Canvas.SetLeft(e, x);
            Canvas.SetTop(e, y);

            canvas.Children.Add(e);
        }

    }
}