<?php
include_once './EUserDataType.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataExtractor
 *
 * @author Matesz
 */
class UserDataExtractor {
    //put your code here
    private $userDataType;
    private $text;
    
    public function __construct($text) {
        
        $this->SetText ($text);
        
    }
    public function SetType ($type)
    {
        $this->userDataType = $type;
    }
    
    public function SetText ($text)
    {
        $this->text = $text;
    }
    
    public function GetValues ($type , $keyWords = null) 
    {
        if (isset ($type))
            $this->userDataType = $type;
        
        switch ($this->userDataType) {
            case EUserDataType::Email:
                return $this->GetEmails ();
            
            case EUserDataType::KeyWord:
                return $this->GetFoundKeyWords ($keyWords);
            
            case EUserDataType::Phone:
                return $this->GetPhoneNumbers ();
            
            default:
                break;
        }
       
    }
    
    private function GetEmails ()
    {
        $matches = array ();
        $pattern = '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i';
        
        preg_match_all ( $pattern
                       , $this->text
                       , $matches
                       );
        $matchArray = array();
 
            foreach ($matches as $value) {
                if(count($value)!=0)
                array_push($matchArray, $value[0]);
            }
        
        
        
        return $matchArray;
    }
    
    private function GetPhoneNumbers ()
    {
        
        $matches = array();
        //30-812-4623  
        //30 812-4623
        preg_match_all( '/[0-9]{2}[\s][0-9]{3}[\-][0-9]{4}|[0-9]{2}[\-][0-9]{3}[\-][0-9]{4}|[0-9]{2}[\s][0-9]{3}[\s][0-9]{4}/'
        //'/[0-9]{3}[\-][0-9]{6}|[0-9]{2}[\s][0-9]{3}[\s][0-9]{4}[\s]|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{9}|[0-9]{2}[\s][0-9]{3}[\-][0-9]{4}|[0-9]{2}[\s][0-9]{3}[\s][0-9]{4}/'
                      , $this->text
                      , $matches
                      );
        //$matches = $matches[0];
        $matchArray = array();

            foreach ($matches as $value) {
                if(count($value)!=0)
                    array_push($matchArray, $value[0]);
            }
        
  
        
        return $matchArray;
    }
    
    private function GetFoundKeyWords ($keyWords) 
    {
        $matches = array();

        
        foreach ($keyWords as $value) {
            $match = null;
            preg_match( '/'.$value.'/i'
                      , $this->text
                      , $match
                      );
            if ( isset ($match) && count($match)!=null)
            {
                array_push($matches, $match[0]);
            }
                      
        }
        //$matches = $matches[0];
        //var_dump($matches);
        return $matches;
    }
}
