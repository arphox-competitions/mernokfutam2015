﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Train
{
    enum PreviousCommandType
    {
        Stop, Move, Open, Close, Over
    }
    static class Client
    {
        static bool reachedFinalDestination;
        static bool gotRoadWork;
        static MTrainClient train;
        static PreviousCommandType prevCommand;
        public static void StartClient()
        {
            Logger.InitializeLogFile();
            // Data buffer for incoming data.
            byte[] bytes = new byte[1024];

            // Connect to a remote device.
            try
            {
                while (!reachedFinalDestination)
                {
                    // Establish the remote endpoint for the socket.
                    // This example uses port 11000 on the local computer.
                    IPHostEntry ipHostInfo = new IPHostEntry();
                    byte[] ipaddress = { 127, 0, 0, 1 };
                    ipHostInfo.AddressList = new IPAddress[1];
                    ipHostInfo.AddressList[0] = new IPAddress(ipaddress);
                    IPAddress ipAddress = ipHostInfo.AddressList[0];
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

                    // Create a TCP/IP  socket.
                    Socket sender = new Socket(AddressFamily.InterNetwork,
                        SocketType.Stream, ProtocolType.Tcp);

                    // Connect the socket to the remote endpoint. Catch any errors.
                    try
                    {
                        sender.Connect(remoteEP);

                        Console.WriteLine("Socket connected to {0}",
                            sender.RemoteEndPoint.ToString());

                        string message = CreateMessage();
                        // Encode the data string into a byte array.
                        byte[] msg = Encoding.ASCII.GetBytes(message);

                        // Send the data through the socket.
                        int bytesSent = sender.Send(msg);

                        // Receive the response from the remote device.
                        int bytesRec = sender.Receive(bytes);
                        string response = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        Console.WriteLine("Echoed test = {0}", response);
                        ProcessResponse(response);
                        // Release the socket.
                        sender.Shutdown(SocketShutdown.Both);
                        sender.Close();
                    }
                    catch (ArgumentNullException ane)
                    {
                        Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                    }
                    catch (SocketException se)
                    {
                        Console.WriteLine("SocketException : {0}", se.ToString());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Unexpected exception : {0}", e.ToString());
                    }
                    Thread.Sleep(200);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static string CreateMessage()
        {
            string message = String.Empty;
            if (prevCommand != PreviousCommandType.Over)
            {
                if (!gotRoadWork)
                {
                    message = "r$";
                }
                else if (train == null)
                {
                    message = "i$";
                }
                else
                {
                    message = String.Format("{0},{1},{2},{3}$", train.X, train.Y, train.DoorState, train.RoadWorkStop);
                    Logger.Log(message.Substring(0, message.Length - 1));
                }
            }
            else
            {
                message = String.Format("{0},{1},{2},{3}$", train.X, train.Y, train.DoorState, 'b');
                Logger.Log(message.Substring(0, message.Length - 1));
            }
            return message;
        }

        static void ProcessResponse(string response)
        {
            string[] s = response.Split(',');
            if (response[0] == 'i')
            {
                train = new MTrainClient(int.Parse(s[1]), int.Parse(s[2]), int.Parse(s[3]));
            }
            else if (response[0] == 's')
            {
                prevCommand = PreviousCommandType.Stop;
            }
            else if (response[0] == 'o')
            {
                train.DoorState = 1;
                prevCommand = PreviousCommandType.Open;
            }
            else if (response[0] == 'c')
            {
                train.DoorState = 0;
                prevCommand = PreviousCommandType.Close;
            }
            else if (response[0] == 'm')
            {
                train.PrevX = train.NextX;
                train.PrevY = train.NextY;
                train.NextX = int.Parse(s[1]);
                train.NextY = int.Parse(s[2]);
                Move(train.Speed);
                prevCommand = PreviousCommandType.Move;
            }
            else if (response[0] == 'n')
            {
                int remaining = Math.Abs(train.NextX - train.X) + Math.Abs(train.NextY - train.Y);
                train.X = train.NextX;
                train.Y = train.NextY;
                train.PrevX = train.NextX;
                train.PrevY = train.NextY;
                train.NextX = int.Parse(s[1]);
                train.NextY = int.Parse(s[2]);
                Logger.Log(String.Format("{0},{1},{2},{3},{4}", train.X, train.Y, train.DoorState, train.RoadWorkStop, 'j'));
                if (s[3] == "p")
                {
                    Move(remaining);
                    prevCommand = PreviousCommandType.Move;
                }
                else
                {
                    prevCommand = PreviousCommandType.Stop;
                }
            }
            else if (response[0] == 'x')
            {
                if (prevCommand == PreviousCommandType.Move)
                {
                    Move(train.Speed);
                }
            }
            else if (response[0] == 'a')
            {
                train.DoorState = 0;
                train.PrevX = train.NextX;
                train.PrevY = train.NextY;
                train.NextX = int.Parse(s[1]);
                train.NextY = int.Parse(s[2]);
                Move(train.Speed);
                prevCommand = PreviousCommandType.Move;
            }
            else if (response[0] == 'z')
            {
                int remaining = Math.Abs(train.NextX - train.X) + Math.Abs(train.NextY - train.Y);
                if (remaining > 0)
                {
                    Move(remaining);
                }
                train.DoorState = 1;
                prevCommand = PreviousCommandType.Over;
            }
            else if (response[0] == 'b')
            {
                reachedFinalDestination = true;
            }
            else if (response[0] == 'w')
            {
                Logger.Log(String.Format("{0},{1},{2}", s[1], s[2], 'r'));
                gotRoadWork = true;
                prevCommand = PreviousCommandType.Stop;
            }
            else if (response[0] == 'r')
            {
                train.RoadWorkStop = 1;
                prevCommand = PreviousCommandType.Stop;
            }
            else if (response[0] == 'q')
            {
                train.RoadWorkStop = 0;
            }
        }

        static void Move(int amount)
        {
            if (train.PrevX == train.NextX)
            {
                train.Y += train.PrevY < train.NextY ? amount : -amount;
            }
            else
            {
                train.X += train.PrevX < train.NextX ? amount : -amount;
            }
        }
    }
}
