<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileType
 *
 * @author Matesz
 */
abstract class EFileType {
    const Pdf     = "pdf";
    const DocDocx = "docdocx";
    const Eml     = "eml";
}
